//
//  ViewController.m
//  OpenWeatherMap_Objective_C
//
//  Created by Kos  on 23/11/15.
//  Copyright © 2015 Kos . All rights reserved.
//

#import "ViewController.h"
#import "OpenWeatherMap.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *url = [NSString stringWithFormat: @"http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=2de143494c0b295cca9337e1e96b00e0"];
    NSURL *stringUrl = [NSURL URLWithString:url];
 
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = (NSURLSessionDataTask *) [session downloadTaskWithURL:stringUrl completionHandler:^(NSURL * location, NSURLResponse * response, NSError * error) {
        
    NSData *weatherData = [[NSData alloc]initWithContentsOfURL:stringUrl];
        NSDictionary *weatherJson = [NSJSONSerialization JSONObjectWithData:weatherData options:0 error: nil];
        
        OpenWeatherMap *weather = [[OpenWeatherMap alloc]init];
        
        NSLog(@"%@", weatherJson[@"name"]);
      
    }];
    
    [task resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
