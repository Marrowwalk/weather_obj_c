//
//  OpenWeatherMap.h
//  OpenWeatherMap_Objective_C
//
//  Created by Kos  on 24/11/15.
//  Copyright © 2015 Kos . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenWeatherMap : NSObject

@property   (nonatomic, strong) NSString *nameCity;
@property   (nonatomic, strong) NSString  *temp;
@property   (nonatomic, strong) NSDictionary *weatherJson;
@property   (nonatomic, strong) NSDictionary *main;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSURL *stringUrl;
@property (nonatomic, strong) NSData *weatherData;

@end
