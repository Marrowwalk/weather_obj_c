//
//  AppDelegate.h
//  OpenWeatherMap_Objective_C
//
//  Created by Kos  on 23/11/15.
//  Copyright © 2015 Kos . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

