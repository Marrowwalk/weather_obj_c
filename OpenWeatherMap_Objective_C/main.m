//
//  main.m
//  OpenWeatherMap_Objective_C
//
//  Created by Kos  on 23/11/15.
//  Copyright © 2015 Kos . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
